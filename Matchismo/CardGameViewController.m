//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Daniel Yim on 3/15/14.
//  Copyright (c) 2014 Daniel Yim. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetGameButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gameModeSegment;
@property (weak, nonatomic) IBOutlet UILabel *flipDescription;
@property (strong, nonatomic) NSMutableArray *flipHistory;
@property (weak, nonatomic) IBOutlet UISlider *historySlider;
@end

@implementation CardGameViewController

- (CardMatchingGame *)game {
    if(!_game) _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                         usingDeck:[self createDeck]];
    self.gameModeSegment.enabled = false;
    return _game;
}
 
- (Deck *)createDeck {
    return [[PlayingCardDeck alloc] init];
}

- (NSMutableArray *)flipHistory {
    if(!_flipHistory) _flipHistory = [[NSMutableArray alloc] init];
    return _flipHistory;
}

- (IBAction)changeSlider:(UISlider *)sender {
    int sliderValue;
    sliderValue = lroundf(self.historySlider.value);
    [self.historySlider setValue:sliderValue animated:NO];
    if ([self.flipHistory count]) {
        self.flipDescription.alpha =
        (sliderValue + 1 < [self.flipHistory count]) ? 0.6 : 1.0;
        self.flipDescription.text =
        [self.flipHistory objectAtIndex:sliderValue];
    }
}


/*
    // Because the slider is "analog", we need to make it have distinct values
    int sliderValue;
    sliderValue = lroundf(sender.value);
    
    [sender setValue:sliderValue animated:NO];
    if(self.flipHistory.count) {
        self.flipDescription.alpha = (sliderValue + 1 < [self.flipHistory count]) ? 0.6 : 1;
        self.flipDescription.text = [self.flipHistory objectAtIndex: sliderValue];
    }
 */


- (IBAction)touchModeSeg:(UISegmentedControl *)sender {
    self.game.maxMatchCards = [[sender titleForSegmentAtIndex:sender.selectedSegmentIndex] integerValue];
    NSLog(@"Game mode changed");
}

// Resets the game (score, card faces, et al.)
- (IBAction)touchResetButton:(UIButton *)sender {
    self.game = nil;
    self.flipHistory = nil;
    [self updateUI];
    self.gameModeSegment.enabled = true;
}

// Defines what happens when the history slider is touched
- (IBAction)touchCardButton:(UIButton *)sender {
    int chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:chosenButtonIndex];
    [self updateUI];
}

- (void)updateUI{
    NSString *status = @"";
    
    for(UIButton *cardButton in self.cardButtons) {
        int cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        
        [cardButton setTitle:[self titleForCard:card]
                    forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card]
                              forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
        
    }
    
    NSMutableArray *chosenContents = [[NSMutableArray alloc] init];
    for (Card *chosenCard in self.game.lastChosenCards) {
        [chosenContents addObject:chosenCard.contents];
    }
    status = [chosenContents componentsJoinedByString:@" "];
    
    if(self.game.lastScore > 0) {
        status = [NSString stringWithFormat:@"Matched %@ for %d points.",
                                status, self.game.lastScore];
    }
    else if(self.game.lastScore < 0) {
        status = [NSString stringWithFormat:@"%@ don't match! %d point penalty!", status, self.game.lastScore];
    }
    self.flipDescription.text = status;
    
    // Add this status to the history
    if(![status isEqualToString:@""] &&
       ![[self.flipHistory lastObject] isEqualToString:status]) {
        [self.flipHistory addObject:status];

         self.historySlider.maximumValue = [self.flipHistory count] - 1;
        [self.historySlider setValue:[self.flipHistory count] - 1
                            animated:YES];
    }
    
    // Disable the game mode switch
    self.gameModeSegment.enabled = false;
}

- (NSString *)titleForCard:(Card *)card {
    return card.isChosen ? card.contents : @"";
}

-(UIImage *)backgroundImageForCard:(Card *)card {
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}

@end
