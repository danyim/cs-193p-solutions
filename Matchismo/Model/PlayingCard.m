//
//  PlayingCard.m
//  Matchismo
//
//  Created by Daniel Yim on 3/15/14.
//  Copyright (c) 2014 Daniel Yim. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

- (NSString *)contents {
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits {
    return @[@"♣️", @"♠️", @"♥️", @"♦️"];
}

- (void)setSuit:(NSString *)suit {
    if([[PlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}

- (NSString *)suit {
    return _suit ? _suit : @"?";
}

+ (NSArray *)rankStrings {
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

+ (NSUInteger)maxRank { return [[self rankStrings] count] - 1; }

- (void)setRank:(NSUInteger)rank {
    if (rank <= [PlayingCard maxRank]) {
        _rank = rank;
    }
}

// This is inherited through class Card and we are overriding it
// Although we can redeclare it in the public API (the header file),
// we don't need to for method overrides.
- (int)match:(NSArray *)otherCards {
    int score = 0;
    int numOtherCards = [otherCards count];
    
    if(numOtherCards) {
        for(PlayingCard *otherCard in otherCards) {
            if(otherCard.rank == self.rank) {
                score += 4;
            }
            else if(otherCard.suit == self.suit) {
                score += 2;
            }
        }
        if(numOtherCards > 1) {
            score += [[otherCards firstObject] match:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards - 1)]];
        }
    }
    
    return score;
}

@end
