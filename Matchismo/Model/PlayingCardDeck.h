//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Daniel Yim on 3/15/14.
//  Copyright (c) 2014 Daniel Yim. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
