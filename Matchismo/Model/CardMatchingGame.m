//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Daniel Yim on 3/15/14.
//  Copyright (c) 2014 Daniel Yim. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

// Only use readwrite to redeclare a public to private
@property (nonatomic, readwrite)NSInteger score;
@property (nonatomic, strong)NSMutableArray *cards; // of Card
@property (nonatomic, readwrite) NSArray *lastChosenCards;
@property (nonatomic, readwrite) NSInteger lastScore;

@end

@implementation CardMatchingGame

// Use either define or const...
// One advantage of const is that it's typed
//#define MISMATCH_PENALTY = 2;
static const int MISMATCH_PENALTY = 2;
static const int MATCH_BONUS = 4;
static const int COST_TO_CHOOSE = 1;

-(NSInteger)maxMatchCards {
    if(_maxMatchCards < 2)
        _maxMatchCards = 2;
    return _maxMatchCards;
}

-(NSMutableArray *)cards {
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

-(instancetype)initWithCardCount:(NSUInteger)count
                       usingDeck:(Deck *)deck {
    self = [super init]; // super's designated initializer
    
    if(self) {
        // Creates a deck of n random cards
        for(int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if(card){
                [self.cards addObject:card];
                // Alternatively...
                // self.cards[i] = card;
            }
            else {
                self = nil;
                break;
            }
        }
    }
    
    return self;
}

-(void)chooseCardAtIndex:(NSUInteger)index {
    Card *card = [self cardAtIndex:index];
    
    if (!card.isMatched) {
        if(card.isChosen) {
            card.chosen = NO;
        }
        else {
            NSMutableArray *chosenCards = [[NSMutableArray alloc] init];
            
            self.lastScore = 0;
            
            for (Card *otherCard in self.cards) {
                // Cycle through the candidates for the currently chosen card
                if(otherCard.isChosen && !otherCard.isMatched) {
                    [chosenCards addObject:otherCard];
                    
                    if([chosenCards count] + 1 == self.maxMatchCards) {
                        int matchScore = [card match:chosenCards];

                        // If we get a match...
                        if(matchScore) {
                            self.lastScore += matchScore * MATCH_BONUS;
                            for (Card *chosenCard in chosenCards) {
                                chosenCard.matched = YES;
                            }
                            card.matched = YES;
                        }
                        else {
                            // Apply the penalty
                            self.lastScore -= MISMATCH_PENALTY;
                            // Then cycle through the chosen cards to flip them
                            // back over
                            for (Card *chosenCard in chosenCards) {
                                chosenCard.chosen = NO;
                            }
                        }
                    }
                }
                
                if([chosenCards count] + 1 == self.maxMatchCards) {
                    // We reached the maximum number of matches, so
                    // quit the algorithm
                    break;
                }
            }
            
            
            self.lastChosenCards = [chosenCards arrayByAddingObject:card];
            self.score += self.lastScore - COST_TO_CHOOSE;
            card.chosen = YES;
        }
    }
}

-(Card *)cardAtIndex:(NSUInteger)index {
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

-(instancetype)init {
    return nil;
}

@end
